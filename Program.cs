﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWSClassRoom;

namespace AWClassRoomClient
{
    class Program
    {                   
        #region private attributes
        #endregion private attributes

        static void Main(string[] args)
        {
            string studentName = "";
            string subnetId = "";

            //TEACHER'S Infra
            studentName = "TEACHER";
            subnetId = "subnet-04aed31590a70f812";
            Console.WriteLine(studentName + " - start");
            try
            {
                String keyPairNameSSH = "LIN3_GRP0_SSHServer";
                String keyPairNameSubnet = "LIN3_GRP0_DCServer";
                List<string> listOfGroupsIdsSSH = new List<string> { "sg-0ce01820a014dfbae" };
                List<string> listOfGroupsIdsSubnet = new List<string> { "sg-02665dbf013552bd7" };
                StudentInstance instSSH = new StudentInstance("ami-05f27d4d6770a43d2", "t2.micro", "10.0.9.10", keyPairNameSSH, listOfGroupsIdsSSH, subnetId, new Tuple<string, string>("Name", "LIN3_GRP0_SSHServer"));
                StudentInstance instDC = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.medium", "10.0.9.11", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP0_DCServer"));
                StudentInstance instFS = new StudentInstance("ami-05f27d4d6770a43d2", "t1.micro", "10.0.9.12", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP0_FileServer"));
                StudentInstance instDB = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.micro", "10.0.9.13", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP0_DBServer"));
                List<StudentInstance> listOfStudentInstances = new List<StudentInstance>();
                listOfStudentInstances.Add(instSSH);
                listOfStudentInstances.Add(instDC);
                listOfStudentInstances.Add(instFS);
                listOfStudentInstances.Add(instDB);
                StudentInfra stud = new StudentInfra(studentName, listOfStudentInstances);

                foreach (StudentInstance stdInst in listOfStudentInstances)
                {
                    InstanceManager instMgr = new InstanceManager("us-east-1");
                    instMgr.CreateInstance(stdInst.AmiId, stdInst.InstanceType, stdInst.PrivateIpAddress, stdInst.KeyPairName, stdInst.GroupsIds, stdInst.SubnetId, stdInst.InstanceName);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                Console.WriteLine();
            }
            Console.WriteLine(studentName + " - end");

            //GROUP 01
            //studentName = "GROUP1";
            //subnetId = "subnet-06324fba358bf557b";
            //Console.WriteLine(studentName + " - start");
            //try
            //{
            //    String keyPairNameSSH = "LIN3_GRP1_SSHServer";
            //    String keyPairNameSubnet = "LIN3_GRP1_SUBNET";
            //    List<string> listOfGroupsIdsSSH = new List<string> { "sg-07261047aba34acd6" };
            //    List<string> listOfGroupsIdsSubnet = new List<string> { "sg-0120e2a1a88fc66a8" };
            //    StudentInstance instSSH = new StudentInstance("ami-05f27d4d6770a43d2", "t2.micro", "10.0.1.10", keyPairNameSSH, listOfGroupsIdsSSH, subnetId, new Tuple<string, string>("Name", "LIN3_GRP1_SRV-SSH-01"));
            //    StudentInstance instDC = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.medium", "10.0.1.11", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP1_SRV-DC-01"));
            //    //StudentInstance instFS = new StudentInstance("ami-05f27d4d6770a43d2", "t1.micro", "10.0.1.12", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP1_SRV-FS-01"));
            //    //StudentInstance instDB = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.micro", "10.0.1.13", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP1_SRV-DB-01"));
            //    List<StudentInstance> listOfStudentInstances = new List<StudentInstance>();
            //    listOfStudentInstances.Add(instSSH);
            //    listOfStudentInstances.Add(instDC);
            //    //listOfStudentInstances.Add(instFS);
            //    //listOfStudentInstances.Add(instDB);
            //    StudentInfra stud = new StudentInfra(studentName, listOfStudentInstances);

            //    foreach (StudentInstance stdInst in listOfStudentInstances)
            //    {
            //        InstanceManager instMgr = new InstanceManager("us-east-1");
            //        instMgr.CreateInstance(stdInst.AmiId, stdInst.InstanceType, stdInst.PrivateIpAddress, stdInst.KeyPairName, stdInst.GroupsIds, stdInst.SubnetId, stdInst.InstanceName);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.ToString());
            //    Console.WriteLine();
            //}
            //Console.WriteLine(studentName + " - end");

            //GROUP 02
            //studentName = "GROUP2";
            //subnetId = "subnet-0726144d8dc51e14b";
            //Console.WriteLine(studentName + " - start");
            //try
            //{
            //    String keyPairNameSSH = "LIN3_GRP2_SSHServer";
            //    String keyPairNameSubnet = "LIN3_GRP2_SUBNET";
            //    List<string> listOfGroupsIdsSSH = new List<string> { "sg-0272e81c867416e79" };
            //    List<string> listOfGroupsIdsSubnet = new List<string> { "sg-0bc8c48d3a26dc4d3" };
            //    StudentInstance instSSH = new StudentInstance("ami-05f27d4d6770a43d2", "t2.micro", "10.0.2.10", keyPairNameSSH, listOfGroupsIdsSSH, subnetId, new Tuple<string, string>("Name", "LIN3_GRP2_SRV-SSH-01"));
            //    StudentInstance instDC = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.medium", "10.0.2.11", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP2_SRV-DC-01"));
            //    //StudentInstance instFS = new StudentInstance("ami-05f27d4d6770a43d2", "t1.micro", "10.0.1.12", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP2_SRV-FS-01"));
            //    //StudentInstance instDB = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.micro", "10.0.1.13", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP2_SRV-DB-01"));
            //    List<StudentInstance> listOfStudentInstances = new List<StudentInstance>();
            //    listOfStudentInstances.Add(instSSH);
            //    listOfStudentInstances.Add(instDC);
            //    //listOfStudentInstances.Add(instFS);
            //    //listOfStudentInstances.Add(instDB);
            //    StudentInfra stud = new StudentInfra(studentName, listOfStudentInstances);

            //    foreach (StudentInstance stdInst in listOfStudentInstances)
            //    {
            //        InstanceManager instMgr = new InstanceManager("us-east-1");
            //        instMgr.CreateInstance(stdInst.AmiId, stdInst.InstanceType, stdInst.PrivateIpAddress, stdInst.KeyPairName, stdInst.GroupsIds, stdInst.SubnetId, stdInst.InstanceName);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.ToString());
            //    Console.WriteLine();
            //}
            //Console.WriteLine(studentName + " - end");

            //GROUP 03
            //studentName = "GROUP3";
            //subnetId = "subnet-068aaa70383dfbddc";
            //Console.WriteLine(studentName + " - start");
            //try
            //{
            //    String keyPairNameSSH = "LIN3_GRP3_SSHServer";
            //    String keyPairNameSubnet = "LIN3_GRP3_SUBNET";
            //    List<string> listOfGroupsIdsSSH = new List<string> { "sg-0f1b3492c7357f773" };
            //    List<string> listOfGroupsIdsSubnet = new List<string> { "sg-08e44ac11451445ff" };
            //    StudentInstance instSSH = new StudentInstance("ami-05f27d4d6770a43d2", "t2.micro", "10.0.3.10", keyPairNameSSH, listOfGroupsIdsSSH, subnetId, new Tuple<string, string>("Name", "LIN3_GRP3_SRV-SSH-01"));
            //    StudentInstance instDC = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.medium", "10.0.3.11", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP3_SRV-DC-01"));
            //    //StudentInstance instFS = new StudentInstance("ami-05f27d4d6770a43d2", "t1.micro", "10.0.1.12", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP3_SRV-FS-01"));
            //    //StudentInstance instDB = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.micro", "10.0.1.13", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP3_SRV-DB-01"));
            //    List<StudentInstance> listOfStudentInstances = new List<StudentInstance>();
            //    listOfStudentInstances.Add(instSSH);
            //    listOfStudentInstances.Add(instDC);
            //    //listOfStudentInstances.Add(instFS);
            //    //listOfStudentInstances.Add(instDB);
            //    StudentInfra stud = new StudentInfra(studentName, listOfStudentInstances);

            //    foreach (StudentInstance stdInst in listOfStudentInstances)
            //    {
            //        InstanceManager instMgr = new InstanceManager("us-east-1");
            //        instMgr.CreateInstance(stdInst.AmiId, stdInst.InstanceType, stdInst.PrivateIpAddress, stdInst.KeyPairName, stdInst.GroupsIds, stdInst.SubnetId, stdInst.InstanceName);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.ToString());
            //    Console.WriteLine();
            //}
            //Console.WriteLine(studentName + " - end");

            //GROUP 04
            //studentName = "GROUP4";
            //subnetId = "subnet-09e859a2e00acaf38";
            //Console.WriteLine(studentName + " - start");
            //try
            //{
            //    String keyPairNameSSH = "LIN3_GRP4_SSHServer";
            //    String keyPairNameSubnet = "LIN3_GRP4_SUBNET";
            //    List<string> listOfGroupsIdsSSH = new List<string> { "sg-06be7516ac666ddab" };
            //    List<string> listOfGroupsIdsSubnet = new List<string> { "sg-0176ceba7c2c8c3e0" };
            //    StudentInstance instSSH = new StudentInstance("ami-05f27d4d6770a43d2", "t2.micro", "10.0.4.10", keyPairNameSSH, listOfGroupsIdsSSH, subnetId, new Tuple<string, string>("Name", "LIN3_GRP4_SRV-SSH-01"));
            //    StudentInstance instDC = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.medium", "10.0.4.11", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP4_SRV-DC-01"));
            //    //StudentInstance instFS = new StudentInstance("ami-05f27d4d6770a43d2", "t1.micro", "10.0.1.12", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP4_SRV-FS-01"));
            //    //StudentInstance instDB = new StudentInstance("ami-0cce25b71e3c7c00f", "t2.micro", "10.0.1.13", keyPairNameSubnet, listOfGroupsIdsSubnet, subnetId, new Tuple<string, string>("Name", "LIN3_GRP4_SRV-DB-01"));
            //    List<StudentInstance> listOfStudentInstances = new List<StudentInstance>();
            //    listOfStudentInstances.Add(instSSH);
            //    listOfStudentInstances.Add(instDC);
            //    //listOfStudentInstances.Add(instFS);
            //    //listOfStudentInstances.Add(instDB);
            //    StudentInfra stud = new StudentInfra(studentName, listOfStudentInstances);

            //    foreach (StudentInstance stdInst in listOfStudentInstances)
            //    {
            //        InstanceManager instMgr = new InstanceManager("us-east-1");
            //        instMgr.CreateInstance(stdInst.AmiId, stdInst.InstanceType, stdInst.PrivateIpAddress, stdInst.KeyPairName, stdInst.GroupsIds, stdInst.SubnetId, stdInst.InstanceName);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.ToString());
            //    Console.WriteLine();
            //}
            //Console.WriteLine(studentName + " - end");

        }


    }
}
